# Generated by Django 4.1.7 on 2023-05-04 18:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('car', '0008_alter_car_details_image_delete_customer'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car_details',
            name='image',
            field=models.ImageField(upload_to='photos/%Y/%m/%d/'),
        ),
    ]
