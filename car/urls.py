from django.urls import path,include
from . import views

app_name = "car"
urlpatterns =[
    path('home',views.home_page, name='home_page'),
    path('cars',views.cars_page, name='cars_page'),
    path('gallery',views.gallery_page, name='gallery_page'),
    path('contactus',views.contactus_page, name='contactus_page'),
    path('aboutus',views.aboutus_page, name='aboutus_page'),
    path('register',views.register_page, name='register_page'),
    path('login',views.login_page, name='login_page'),
    path('logout',views.logout_page, name='logout_page'),
    # path('login_view',views.login_view_page, name='login_view_page'),
    path('dashboard',views.dashboard_page, name='dashboard_page'),

           
]