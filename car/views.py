from django.shortcuts import render, redirect
from . models import Car_details
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import User

def home_page(request):
    return render(request,'car/home.html')

def cars_page(request):
    car_details = Car_details.objects.all()
    return render(request,'car/cars.html',{'car_details': car_details})

def gallery_page(request):
    return render(request,'car/gallery.html')

def contactus_page(request):
    return render(request,'car/contactus.html')

def aboutus_page(request):
    return render(request,'car/aboutus.html')

def register_page(request):
    if request.method == "POST":
        firstname_ = request.POST.get('firstname','')
        lastname_ = request.POST.get('lastname','')
        username_ = request.POST.get('username','')
        email_ = request.POST.get('Email','')
        password_ = request.POST.get('password','')
        confirm_password_ = request.POST.get('confirm_password','')
        
        if password_ == confirm_password_:
            if User.objects.filter(username = username_).exists():
                messages.error(request, 'username already exists')
                return redirect('car:register_page')
            else:
                if User.objects.filter(email = email_).exists():
                    messages.error(request, 'Email already exists')
                    return redirect('car:register_page')
                else:
                    user = User.objects.create_user(username_,email_,password_)
                    user.first_name = firstname_
                    user.last_name = lastname_
                    user.save()
                   
                    return redirect('car:register_page')
                
    return render(request, 'car/register.html')
        
        
    

def login_page(request):
    if request.method == "POST":
        username_ = request.POST.get('user','')
        password_ = request.POST.get('pass','')
        user = authenticate(request, username = username_, password = password_ )
        if user is not None:
            login(request, user)
            return redirect('car:home_page')
        else:
            return redirect('car:login_page')
    return render(request, 'car/login.html')



def logout_page(request):
    return render(request, 'car/dashboard.html')

def dashboard_page(request):
    return render(request, 'car/dashboard.html')